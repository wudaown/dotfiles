set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif
" ---------------------------- Own Setting --------------------------------
filetype plugin indent on
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set nu
set rnu
set smartindent
set autoindent
set hlsearch
set incsearch
set showmatch
set nocompatible
set nobk
syntax on
set background=dark
" set guifont=Ubuntu\ Mono\ derivative\ Powerline:h13
set fenc=utf-8
set fileencodings=utf-8,cp936,big5,euc-jp,euc-kr,latin1,ucs-bom
set keywordprg=sdcv
set backspace=2
set showcmd		" display incomplete commands

" Enable folding
set foldmethod=syntax
set foldmethod=indent
" autocmd Syntax c,cpp,vim,xml,html,xhtml setlocal foldmethod=syntax
" autocmd Syntax c,cpp,vim,xml,html,xhtml,perl normal zR
set foldlevelstart=20
" set foldlevel to 0 will close all fold, foldlevel=20 will close > 20 lines

" Start Quick Run
map <F5> :call CompileRunGcc()<CR>
" map <F1> :ProjectCreate . -n java<CR>
func! CompileRunGcc()
exec "w"
if &filetype == 'c'
exec "!gcc % -o %<"
exec "!time ./%<"
elseif &filetype == 'cpp'
exec "!g++ % -o %<"
exec "!time ./%<"
elseif &filetype == 'java'
exec "!javac %"
exec "!time java -cp %:p:h %:t:r"
elseif &filetype == 'sh'
exec "!time bash %"
elseif &filetype == 'python'
exec "!time python3 %"
elseif &filetype == 'html'
exec "!firefox % &"
elseif &filetype == 'go'
exec "!go build %<"
exec "!time go run %"
elseif &filetype == 'mkd'
exec "!~/.vim/markdown.pl % > %.html &"
exec "!firefox %.html &"
endif
endfunc

" indentation for python
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

" indentation for web development
au BufNewFile,BufRead *.js,*.html,*.css,*.yml
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set expandtab 

" Add header for py and sh file
autocmd BufNewFile *.py call ScriptHeader()
autocmd BufNewFile *.sh call ScriptHeader()
autocmd BufNewfile *.c call ScriptHeader()
autocmd BufNewfile *.yml call ScriptHeader()
function ScriptHeader()
    if &filetype == 'python'
        let header = "#!/usr/bin/env python3"
        let coding = "# _*_ coding:utf8 _*_"
    elseif &filetype == 'sh'
        let header = "#!/bin/bash"
    elseif &filetype == "c"
        let header = "#include <stdio.h>"
	elseif &filetype == 'yaml'
		let header = '---'
		let nextline = ''
    endif
    let line = getline(1)
    if line == header
        return
    endif
    normal m'
    call append(0,header)
    if &filetype == 'python'
        call append(1,coding)
    endif
	if &filetype == 'yaml'
		call append(1,nextline)
	endif
    normal ''
endfunction

" imap jj <Esc>
" inoremap <c-s> <Esc>:update<CR>
" inoremap <c-s> <c-o>:Update<CR><CR>
map - <c-w>-
map = <c-w>+
map < <c-w><
map > <c-w>>
" switch windows without folding
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" switch windows with folding
" map <C-J> <C-W>j<C-W>_
" map <C-K> <C-W>k<C-W>_
" map <C-H> <C-W>h<C-W>_
" map <C-L> <C-W>l<C-W>_
nmap <F8> :TagbarToggle<CR>
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>
" save and return to insert mode
"inoremap <c-s> <c-o>:update<CR>
noremap <space> :
nmap <C-Q> :q!<CR>
"au VimEnter * !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
"au VimLeave * !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'



" ---- Vundle
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'majutsushi/tagbar'
Plugin 'vim-scripts/SingleCompile'
Plugin 'dylanaraps/wal.vim'
Plugin 'w0rp/ale'
Plugin 'davidhalter/jedi-vim'
" Plugin 'nvie/vim-flake8'
Plugin 'ervandew/supertab'
Plugin 'Raimondi/delimitMate'
" Plugin 'SirVer/ultisnips'
" Plugin 'honza/vim-snippets'
" Plugin 'Valloric/YouCompleteMe'
Plugin 'skywind3000/asyncrun.vim'
" Plugin 'vim-latex/vim-latex'
" Plugin 'xuhdev/vim-latex-live-preview'
Plugin 'scrooloose/nerdtree'
Plugin 'Yggdroot/IndentLine'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'NLKNguyen/papercolor-theme'
Plugin  'altercation/vim-colors-solarized'
call    vundle#end()

" ---- nerdtree
map <F2> :NERDTreeToggle<CR>
let NERDTreeChDirMode=1
let NERDTreeShowBookmarks=1
let NERDTreeIgnore=['\~$', '\.pyc$', '\.swp$']
let NERDTreeWinSize=25

" ---- indentLine
"缩进指示线"
let g:indentLine_char='┆'
let g:indentLine_enabled = 1


" ---- airline
let g:airline_powerline_fonts = 1
" 是否启用顶部tabline
let g:airline#extensions#tabline#enabled = 1
" 顶部tabline显示方式
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
set laststatus=2
" nnoremap ii   :bnext<CR>
" nnoremap ii   :tabnext<CR>

set t_Co=256   " This is may or may not needed.
" colorscheme PaperColor
" colorscheme solarized
colorscheme wal
hi Normal guibg=NONE ctermbg=NONE


set clipboard=unnamedplus
autocmd Filetype tex setl updatetime=1
let g:livepreview_previewer = 'evince'
" set spell spelllang=en_us
" 
if has('gui_running')
    "set guifont=Ubuntu\ Mono\ 13
    set guifont=xos4\ Terminus\ 12
    colorscheme PaperColor
    "colorscheme solarized
endif
let g:SuperTabDefaultCompletionType = "context"
let g:jedi#popup_on_dot = 0
